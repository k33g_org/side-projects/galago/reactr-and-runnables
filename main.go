package main

import (
	"fmt"
	"github.com/suborbital/reactr/rt"
	"github.com/suborbital/reactr/rwasm"
)


func main() {
	r := rt.New()
	doHelloWasm := r.Register("hello", rwasm.NewRunner("./hello/hello.wasm"))

	result, err := doHelloWasm([]byte("Bob Morane!")).Then()
	if err != nil {
		fmt.Println(err)
		return
	}
	fmt.Println(string(result.([]byte)))

}
