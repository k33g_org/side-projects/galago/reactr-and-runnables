# ebook go samples Reactr


## Create and build a Runnable

```bash
# create the runnable
subo create runnable hello --lang tinygo
# build the runnable
subo build hello/
```

## Create and build the Runnable launcher

```bash
# initialize the launcher
go mod init play-with-reactr
touch main.go
```

Use `go mod tidy` to update (and the dependencies) the dependencies
